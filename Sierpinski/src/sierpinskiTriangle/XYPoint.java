package sierpinskiTriangle;

	class XYPoint{

		private double x;
		private double y;
		
		public XYPoint() {
			super();
		}

		public XYPoint(double x, double y) {
			super();
			this.setX(x);	
			this.setY(y);
		}

		public double getX() {
			return x;
		}
		
		public void setX(double x) {
			this.x = x;
		}
		
		public double getY() {
			return y;
		}
		
		public void setY(double y) {
			this.y = y;
		}
		
		public double[] getXY() {
			double [] XYPoints = new double[2];
			XYPoints[0] = getX();
			XYPoints[1] = getY();
			return XYPoints;
		}
		
		
		@Override
		public String toString() {
			return "["+ getX() + " " + getY() + "]";
		}
		
	}