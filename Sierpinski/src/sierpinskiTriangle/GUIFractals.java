package sierpinskiTriangle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TitledPane;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GUIFractals extends Application {

	private Rectangle2D screenSize;
	private Button btnShowFractal;
	private Button btnChooseSierp;
	private Button btnCloseFrac;
	private FlowPane root;
	private Scene scene;
	private Scene scene2;
	private Spinner<Integer> spin;
	// private SpinnerValueFactory<Integer> spinVal;
	private IntegerSpinnerValueFactory spinVal;
	private Label lblIteration;
	private Label lblChooseColorBase;
	private Label lblChooseColorIter;
	
	private TitledPane tldPane;
	private VBox content;
	private double factor;
	private double width;
	private double height;
	private Stage primaryStage;
	private int iteration;

	// for sierpinski
	private List<XYPoint> triPointList;
	private List<XYPoint> triPointList2;
	private List<Triangle> TriangleList;
	private List<XYPoint> trianglePointsGet;
	private List<XYPoint> triangleStartPointsLast;

	private double xMaxDiv = 4.0; // 4.0
	private double yMaxDiv = 2.0; // 2.0
	private Random rand;
	private Color color;
	private ColorPicker colorPickerIter;
	private ColorPicker colorPickerBase;
	private VBox colPickIter;
	private VBox colPickBase;

	List<XYPoint> XYNew;

	XYPoint xyPoint1;
	XYPoint xyPoint2;
	XYPoint xyPoint3;
	
	// for second stage
	Stage stage2;

	@Override
	public void start(Stage primaryStageIn) throws Exception {

		primaryStage = primaryStageIn;

		// gibt komplette Monitor-Aufl�sung (ist wie die Aufl�sung durch Graka)
		screenSize = Screen.getPrimary().getBounds();
		System.out.println(screenSize.getWidth() + "\t" + screenSize.getHeight());

		factor = 2;
		width = screenSize.getWidth() / factor;
		height = screenSize.getHeight() / factor;

		
		// Buttons
		// start button
		btnShowFractal = new Button("Show Fractal");
		btnShowFractal.setPrefSize(150, 100);
		btnShowFractal.setOnAction(event -> showSierpinski());

		// choose sierpinski button
		btnChooseSierp = new Button("Sierpinskie-Triangle");
		btnChooseSierp.setStyle("-fx-background-color: cyan");
		btnChooseSierp.setOnAction(event -> getSpinVal());
		
		// here close button for second stage (the fractal view)
		btnCloseFrac = new Button("close fractal view");
		btnCloseFrac.setPrefSize(150, 50);
		btnCloseFrac.setOnAction(event -> closeFrac());
		
		// ColorPicker
		// here ColorPicker (works like all the other nodes)
		colorPickerIter = new ColorPicker();
		colorPickerIter.setValue(Color.BLUE);
		
		// colorPickerBase
		colorPickerBase = new ColorPicker();
		colorPickerBase.setValue(Color.BLACK);
		
		// Labels
		lblIteration = new Label("Select Iterations (max 10):");
		lblChooseColorBase = new Label("Color for base triangle");
		lblChooseColorIter = new Label("Color iteration");
		
		root = new FlowPane();
		spin = new Spinner<Integer>();
		/*
		 * this sets the limits and starting value for Spinner SpinnerValueFactory is an
		 * abstract class you cannot instantiate it but you can address its methods
		 * directly
		 */
		// we allow max 10 iterations

		/*
		 * IntegerSpinnerValueFactory is a static class within SpinnerValueFactory we
		 * create an object of it by calling IntegerSpinnerValueFactory() the reference
		 * works because: IntegerSpinnerValueFactory extends SpinnerValueFactory or we
		 * use "IntegerSpinnerValueFactory" directly (not sure how this affects other
		 * processes oO)
		 */
		
		spinVal = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 10);

		// System.out.println(spinVal.getClass().getSimpleName());
		// here the values for Spinner are set
		spin.setValueFactory(spinVal);

		tldPane = new TitledPane();
		tldPane.setText("Fractal-Types");

		// we can use VBox Content for TitledPane (yes, there is also a HBox ^^')
		content = new VBox();
		content.getChildren().add(btnChooseSierp);
		content.getChildren().add(new Label("more coming soon.. "));
		tldPane.setContent(content);

		// vertical Boxes for alignment of colorpickers and its labels
		
		colPickIter = new VBox();
		colPickBase = new VBox();
		
		int paddingSizeColBox = 5;
		int spacingVal = 10;
		colPickIter.setPadding(new Insets(paddingSizeColBox,paddingSizeColBox,paddingSizeColBox,paddingSizeColBox));
//		colPickIter.setStyle("-fx-background-color: cyan"); // ist hier nur um zu sehen wie sich das padding auswirkt
		colPickIter.setSpacing(spacingVal); // this influences the Distance between the vertical aligned nodes 

		
		colPickBase.setPadding(new Insets(paddingSizeColBox,paddingSizeColBox,paddingSizeColBox,paddingSizeColBox));
		colPickBase.setSpacing(spacingVal);
		
		/* use setAlignment() to  "center" so the label is in the center on top of the Colopicker below
		 * Pos is an Enum with many position constants
		 */
		colPickIter.setAlignment(Pos.CENTER); 
		colPickBase.setAlignment(Pos.CENTER); 
		
		colPickIter.getChildren().addAll(lblChooseColorIter, colorPickerIter);
		colPickBase.getChildren().addAll(lblChooseColorBase, colorPickerBase);
		
		root = new FlowPane();
		root.setHgap(50);
		root.setVgap(50);
		int paddingSizeRoot = 20;
		// setPadding influences the distance of the nodes to the Pane edge
		root.setPadding(new Insets(paddingSizeRoot,paddingSizeRoot,paddingSizeRoot,paddingSizeRoot)); 

		root.getChildren().addAll(tldPane, lblIteration, spin,
								  btnShowFractal, btnCloseFrac,								  
								  colPickBase,
								  colPickIter
								  );

		scene = new Scene(root, width, height);

		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public void showSierpinski() {
		
		trianglePointsGet = new ArrayList<>();
		triangleStartPointsLast = new ArrayList<XYPoint>();
		triPointList = new ArrayList<>();		
		triPointList2 = new ArrayList<>();		
		TriangleList = new ArrayList<>(); // hier initialisierung der hauptliste
		
		// r�cksetzen dieser Werte bei neuem start super-wichtig
		xMaxDiv = 4.0;
		yMaxDiv = 2.0;
		
		System.out.println("width/ height Relation: " + width/height
						   + "\nheight/ width Relation: " + height/width);
				
		Group root2 = new Group();
		scene2 = new Scene(root2, width, height, Color.WHITE);
		// New window (Stage)
		/* if case here important otherwise countless windows can be opened
		 * here only one at the time is allowed
		 * only the last opened stage is reached by close() or hide() 
		 * if you want several stages you need to save their addresses in some array o.s.e
		 * and maybe restrict their amount  
		 */
		if(stage2 != null){
			stage2.close();
			stage2 = new Stage();
		}else {
			stage2 = new Stage();
		}
				
		stage2.setTitle("Second Stage");
		stage2.setScene(scene2);

		// Set position of second window, related to primary window.
		stage2.setX(primaryStage.getX() + 200);
		stage2.setY(primaryStage.getY() + 200);
//		stage2.initStyle(StageStyle.TRANSPARENT);
		stage2.initStyle(StageStyle.UNDECORATED);
		stage2.show();

		iteration = getSpinVal(); // hier Anzahl an iteration setzen (hier wert von Spinner)
		
		// triangleStartPointsLast = new ArrayList<>();
		int newTriAmount = 1;
		int timesNewTriAmount = 3; // 3
		int totalTriAmount = 0; // total amount of triangles
		int resFactor = 4; // this value is doubled at each iteration; it impacts the size of the triangles
		boolean addPosFac = false; // use this to communicate to createThreeTriangles-method if position-factor
									// should be doubled or not
		for (int i = 0; i <= iteration; i++) {

			switch (i) {
			case 0:
				firstTriangle(width, height);
				TriangleList.add(new Triangle(triPointList)); // 1.triangle
				color = colorPickerBase.getValue();
				TriangleList.get(i).getP().setFill(color);
				root2.getChildren().add(TriangleList.get(i).getP());
				totalTriAmount+=1;
				System.out.println("totalTriAmount: " + totalTriAmount);
				break;
			case 1:
				secondTriangle(width, height);
				TriangleList.add(new Triangle(triPointList));
				System.out.println(TriangleList);
				rand = new Random();
//				color = Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
				color = colorPickerIter.getValue();
				TriangleList.get(i).getP().setFill(color);
				root2.getChildren().add(TriangleList.get(i).getP());
				totalTriAmount+=1;
				System.out.println("totalTriAmount: " + totalTriAmount);
				break;
			default:
				int addNew = 0;
				addPosFac = true;
				trianglePointsGet = new ArrayList<>();
				int subL1 = 0;
				int subL2 = 3;
				while (addNew < newTriAmount * timesNewTriAmount) {
					if (i == 2) { // special case (we can use triPointList here because there was only one new
									// triangle in the iteration before)
						createThreeTriangles(triPointList, addPosFac);
					} else {
						createThreeTriangles(triangleStartPointsLast.subList(subL1, subL2), addPosFac);
					}
//					System.out.println("triPointList2 " + triPointList2); // triPointList2 has 3 XYPoint which define
																			// the start-position of the new triangles
					addPosFac = false;
					Polygon p1;
					for (XYPoint xyPoint : triPointList2) {
						TriangleAuto(xyPoint, resFactor); // triPointList wird hier immer ge�ndert --> eine neue
															// triPointList = ein neues triangle
						p1 = new Triangle(triPointList).getP();
						rand = new Random();
//						color = Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
						color = colorPickerIter.getValue();
						p1.setFill(color);
						root2.getChildren().add(p1);
						trianglePointsGet.addAll(triPointList);
					}

					addNew += 3;
					subL1 += 3;
					subL2 += 3;
				}
				triangleStartPointsLast = new ArrayList<XYPoint>(trianglePointsGet);
				resFactor *= 2;
				newTriAmount *= timesNewTriAmount;
				totalTriAmount+=newTriAmount;
				System.out.println("newTriAmount: " + newTriAmount);
				System.out.println("totalTriAmount: " + totalTriAmount);
				break;
			}

		}

	}

	public int getSpinVal() {
		int currIterVal = spin.getValue();
		System.out.println(currIterVal);
		return spin.getValue();
	}

	// here main-method
	public static void main(String[] args) {

		launch(args);
	}
	
	public void closeFrac() {
		stage2.close(); // close is equivalent is hide() oO
	}

	// Methods for Sierpinski
	// method to create first triangle; the size of the first Polygon depends on the
	// screen-size
	void firstTriangle(double xMaxIn, double yMaxIn) {
		triPointList = new ArrayList<>();
		this.triPointList.add(new XYPoint(0.0, yMaxIn));
		this.triPointList.add(new XYPoint(xMaxIn, yMaxIn));
		this.triPointList.add(new XYPoint(xMaxIn / 2, 0.0));
	}

	// method for the second triangle
	void secondTriangle(double xMaxIn, double yMaxIn) {
		triPointList = new ArrayList<>();
		this.triPointList.add(new XYPoint(xMaxIn / 4, yMaxIn / 2));
		this.triPointList.add(new XYPoint(xMaxIn / 2, yMaxIn));
		this.triPointList.add(new XYPoint(xMaxIn -= xMaxIn / 4, yMaxIn / 2));
	}

	void createThreeTriangles(List<XYPoint> xy, boolean addPosDouble) {
		/*
		 * pseudo-code: get ArrayList of last 3 triangles use values 0 and 2 of the last
		 * 3 triangles to create 3 new triangles for each of the last 3 triangles give
		 * back the new Array with the new 3 triangles
		 */

		boolean addPosDoub = addPosDouble;
		/*
		 * hier xMaxDiv und yMaxDiv verdoppeln bei jeder iteration (ab iteration 2)
		 * daf�r boolean addPosDoub auf true wenn der Divisor f�r die Positionen
		 * verdoppelt werden soll
		 */

		if (addPosDoub) {
			this.xMaxDiv *= 2;
			this.yMaxDiv *= 2;
		}


		triPointList2 = new ArrayList<>();

		// get Points triangle left
		double xyPointT2LX = xy.get(0).getX();
		double xyPointT2LY = xy.get(0).getY();

		// System.out.println("xyPointT2LX " + xyPointT2LX);

		// get Points triangle right
		double xyPointT2RX = xy.get(2).getX();
		double xyPointT2RY = xy.get(2).getY();

		// get Points triangle top (same as for triangle left)
		double xyPointT2OX = xy.get(0).getX();
		double xyPointT2OY = xy.get(0).getY();

		// ---- recalc xy-Positions ---
		// triangle left
		// if (addPosDoub) {
		xyPointT2LX -= (width / xMaxDiv); // here double denominator value (iter before: 6)
		xyPointT2LY += (height / yMaxDiv); // iter before: 4

		// triangle right
		xyPointT2RX -= (width / xMaxDiv);
		xyPointT2RY += (height / yMaxDiv);

		// triangle top
		xyPointT2OX += (width / xMaxDiv);
		xyPointT2OY -= (height / yMaxDiv);
		// }
		XYPoint TLeft = new XYPoint(xyPointT2LX, xyPointT2LY);

		// System.out.println(TLeft.toString() + "\t hier in Klasse" );
		XYPoint TRight = new XYPoint(xyPointT2RX, xyPointT2RY);
		XYPoint TTop = new XYPoint(xyPointT2OX, xyPointT2OY);

		triPointList2.add(TLeft);
		triPointList2.add(TRight);
		triPointList2.add(TTop);

	}

	void TriangleAuto(XYPoint xyPointIn, int sizeR) {
		triPointList = new ArrayList<>();

		// this.xMax = primaryScreenBounds.getHeight()/2;
		// this.yMax = primaryScreenBounds.getWidth()/2;
		double xMax = scene2.getWidth();
		double yMax = scene2.getHeight();

		XYPoint xyPoint1 = xyPointIn;

		// first xyPoint set positions; the too others are just to set the size of the
		// triangle
		double xy2X = xyPointIn.getX() + (xMax / (sizeR * 2));
		double xy2Y = xyPointIn.getY() + (yMax / (sizeR));
		XYPoint xyPoint2 = new XYPoint(xy2X, xy2Y);

		double xy3X = xyPointIn.getX() + (xMax / (sizeR));
		double xy3Y = xyPointIn.getY(); // Y-axis-value does not change
		XYPoint xyPoint3 = new XYPoint(xy3X, xy3Y);

		// here ArrayList for Polygon-Object with 3 Points
		triPointList.add(xyPoint1);
		triPointList.add(xyPoint2);
		triPointList.add(xyPoint3);
	}

}
