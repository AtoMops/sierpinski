package sierpinskiTriangle;

import java.util.List;
import javafx.scene.shape.Polygon;

// class Triangle takes an ArrayList with 3 XYPoints and creates a (Triangle-)Polygon-object
class Triangle{
	private Polygon p = new Polygon();
	
	Triangle(List<XYPoint> myPointList) throws IllegalArgumentException{
		p = new Polygon();
		
		 if(myPointList.size() != 3) {
			 throw new IllegalArgumentException("Die Punkte-Liste muss genau 3 Punkt-Objekte enthalten: " 
					 							+ "Listen-Gr��e ist: " + myPointList.size());
		 }
		
		for (XYPoint xyPoint : myPointList) {
			p.getPoints().add(xyPoint.getX());
			p.getPoints().add(xyPoint.getY());
		}
	}
	
	public Polygon getP() {
		return p;
	}
	
	@Override
	public String toString() {
		return "Polygon-XY-Points=" + p.getPoints() + "]";
	}
	
}